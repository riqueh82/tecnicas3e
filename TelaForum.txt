package com.example.seethesea;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class TelaForum extends AppCompatActivity// Classe TelaForum herda AppCompatActivity
{
    @Override //Implementa��o de um m�todo j� criado - Sobrescrita
    protected void onCreate(Bundle savedInstanceState) // M�todo OnCreate -Inicializa a atividade; Bundle - Respons�vel por guardar o estado da Activity (cache)
    {
        super.onCreate(savedInstanceState); // Faz rodar o c�digo junto com o c�digo da classe pai
        setContentView(R.layout.activity_main); // � respons�vel por configurar o layout da Activity Principal (Tela Inicial) (Por enquanto t� no Main, mas depois muda)
    }
}