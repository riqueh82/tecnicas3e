package com.example.seethesea;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;



public class TelaAjude extends AppCompatActivity // Classe TelaAjude herda a AppCompatActivity
{


    @Override //Implementa��o de um m�todo j� criado - Sobrescrita
    protected void onCreate(Bundle savedInstanceState) // M�todo OnCreate -Inicializa a atividade; Bundle - Respons�vel por guardar o estado da Activity (cache)
    {
        super.onCreate(savedInstanceState); // Faz rodar o c�digo junto com o c�digo da classe pai
        setContentView(R.layout.activity_tela_ajude); // � respons�vel por configurar o layout da Activity

    }
}